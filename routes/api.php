<?php

use App\Services\BaseService;

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$router->group(['namespace' => 'Api\v1', 'prefix' => 'api'], function($router)  {
    $router->group(['prefix' => 'user'], function($router)  {

        $router->post('/register', 'AuthController@create');
        $router->post('/sign-in', 'AuthController@login');
        $router->post('/recover-password', ['uses' => 'AuthController@resetPassword', 'as' => 'password.reset']);

        $router->group(['middleware' => 'jwt.auth'], function($router) {

            $router->get('/logout', 'AuthController@logout');
            $router->get('/send-recover-password', 'AuthController@generateResetToken');

            $router->get('/companies', 'CompanyController@index');
            $router->post('/companies', 'CompanyController@create');
        });

    });

});
