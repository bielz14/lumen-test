<?php

use Illuminate\Support\Str;

$newPassword = Str::random(6);
$url .= '&password=' . $newPassword;

?>
<table class="action" align="center" width="100%" cellpadding="0" cellspacing="0" role="presentation">
<tr>
<td align="center">
<table width="100%" border="0" cellpadding="0" cellspacing="0" role="presentation">
<tr>
<td align="center">
<table border="0" cellpadding="0" cellspacing="0" role="presentation">
<tr>
<td>
    <span style="margin: 0 2% 2% 0; font-weight: bold">Your new password be:</span>
    <span style="margin: 1% 0 4% 24%; font-weight: bold">{{ $newPassword }}</span>
<form action="{{ $url }}" method="POST">
    <button type="submit" class="button button-{{ $color ?? 'primary' }}">{{ $slot }}</button>
</form>
<!--<a href="{ { $url }}" class="button button-{ { $color ?? 'primary' }}" target="_blank" rel="noopener">{ { $slot }}</a>-->
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
