<?php

namespace App\Services;

use App\Models\Token;

class TokenService extends BaseService
{
    public function __construct()
    {
        parent::__construct(Token::class);
    }
}
