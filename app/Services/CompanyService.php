<?php

namespace App\Services;

use App\Models\Company;

class CompanyService extends BaseService
{
    public function __construct()
    {
        parent::__construct(Company::class);
    }

    #region Company

    /**
     * @param int|array|\Closure $conditions
     * @param null               $withRelations
     * @param array|string       $limitOffset
     * @param array|string       $ordering
     *
     * @return Company|\App\Models\BaseModel|Company|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\Relation|null|object
     */
    public function getCompany($conditions = [], $withRelations = null, $limitOffset = [], $ordering = [], $orderingType = null) : ?Company
    {
        return $this
            ->getBuilder($this->prepareConditions($conditions), $withRelations, $limitOffset, $ordering, $orderingType)
            ->first();
    }

    /**
     * @param int|array|\Closure $conditions
     * @param null               $withRelations
     * @param array|string       $limitOffset
     * @param array|string       $ordering
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getCompanies(
        $conditions = [],
        $withRelations = null,
        $limitOffset = [],
        $ordering = [],
        $orderingType = null
    ) : \Illuminate\Database\Eloquent\Collection {
        return $this
            ->getBuilder($this->prepareConditions($conditions), $withRelations, $limitOffset, $ordering, $orderingType)
            ->get();
    }

    /**
     * @param int|array|\Closure $conditions
     * @param null               $withRelations
     * @param array|string       $limitOffset
     * @param array|string       $ordering
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getCompaniesPaginate(
        $pages = 1,
        $conditions = [],
        $withRelations = null,
        $limitOffset = [],
        $ordering = [],
        $orderingType = null
    ) : \Illuminate\Contracts\Pagination\LengthAwarePaginator {
        return $this
            ->getBuilder($this->prepareConditions($conditions), $withRelations, $limitOffset, $ordering, $orderingType)
            ->paginate($pages);
    }

    /**
     * @param int|array|\Closure $conditions
     * @param string             $name
     * @param null               $withRelations
     * @param array|string       $limitOffset
     * @param array|string       $ordering
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function getCompaniesWithLikeTitle(
        $name,
        $conditions = [],
        $withRelations = null,
        $limitOffset = [],
        $ordering = [],
        $orderingType = null
    ) : \Illuminate\Database\Eloquent\Collection {

        array_push($conditions, ['title', 'like', '%' . $name . '%']);

        return $this
            ->getBuilder($this->prepareConditions($conditions), $withRelations, $limitOffset, $ordering, $orderingType)
            //->where('name', 'like', '%' . $name . '%')
            ->get();
    }

    /**
     * @param array $attributes
     *
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Support\Collection|null
     */
    public function createCompany(array $attributes, ?string $className = Company::class)
    {
        return $this->createOneOrMany($attributes, $className);
    }

    /**
     * @param Company $company
     * @param array   $attributes
     *
     * @return int|null
     */
    public function updateCompany(Company $company, array $attributes) : ?int
    {
        return $this->updateOneAuthenticable($company, $attributes);
    }

    /**
     * @param int|array|\Closure $conditions
     * @param bool               $force
     *
     * @return bool
     */
    public function deleteCompany($conditions, bool $force = false) : bool
    {
        return $this->delete($this->prepareConditions($conditions), $force);
    }
}
