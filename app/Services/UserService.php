<?php

namespace App\Services;

use App\Models\Token;
use App\Models\User;

class UserService extends BaseService
{
    public function __construct()
    {
        parent::__construct(User::class);
    }

    #region User

    /**
     * @param int|array|\Closure $conditions
     * @param null               $withRelations
     * @param array|string       $limitOffset
     * @param array|string       $ordering
     *
     * @return User|\App\Models\BaseModel|User|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\Relation|null|object
     */
    public function getUser($conditions = [], $withRelations = null, $limitOffset = [], $ordering = [], $orderingType = null) : ?User
    {
        return $this
            ->getBuilder($this->prepareConditions($conditions), $withRelations, $limitOffset, $ordering, $orderingType)
            ->first();
    }

    /**
     * @param int|array|\Closure $conditions
     * @param null               $withRelations
     * @param array|string       $limitOffset
     * @param array|string       $ordering
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getUsers(
        $conditions = [],
        $withRelations = null,
        $limitOffset = [],
        $ordering = [],
        $orderingType = null
    ) : \Illuminate\Database\Eloquent\Collection {
        return $this
            ->getBuilder($this->prepareConditions($conditions), $withRelations, $limitOffset, $ordering, $orderingType)
            ->get();
    }

    /**
     * @param int|array|\Closure $conditions
     * @param null               $withRelations
     * @param array|string       $limitOffset
     * @param array|string       $ordering
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getUsersPaginate(
        $pages = 1,
        $conditions = [],
        $withRelations = null,
        $limitOffset = [],
        $ordering = [],
        $orderingType = null
    ) : \Illuminate\Contracts\Pagination\LengthAwarePaginator {
        return $this
            ->getBuilder($this->prepareConditions($conditions), $withRelations, $limitOffset, $ordering, $orderingType)
            ->paginate($pages);
    }

    /**
     * @param int|array|\Closure $conditions
     * @param string             $name
     * @param null               $withRelations
     * @param array|string       $limitOffset
     * @param array|string       $ordering
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function getUsersWithLikeName(
        $name,
        $conditions = [],
        $withRelations = null,
        $limitOffset = [],
        $ordering = [],
        $orderingType = null
    ) : \Illuminate\Database\Eloquent\Collection {

        array_push($conditions, ['name', 'like', '%' . $name . '%']);

        return $this
            ->getBuilder($this->prepareConditions($conditions), $withRelations, $limitOffset, $ordering, $orderingType)
            //->where('name', 'like', '%' . $name . '%')
            ->get();
    }

    /**
     * @param array $attributes
     *
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Support\Collection|null
     */
    public function createUser(array $attributes, ?string $className = User::class)
    {
        return $this->createOneOrMany($attributes, $className);
    }

    /**
     * @param User    $user
     * @param array   $attributes
     *
     * @return int|null
     */
    public function updateUser(User $user, array $attributes) : ?int
    {
        return $this->updateOneAuthenticable($user, $attributes);
    }

    /**
     * @param int|array|\Closure $conditions
     * @param bool               $force
     *
     * @return bool
     */
    public function deleteUser($conditions, bool $force = false) : bool
    {
        return $this->delete($this->prepareConditions($conditions), $force);
    }

        #region Token

    /**
     * @param int|array|\Closure $conditions
     * @param null               $withRelations
     * @param array|string       $limitOffset
     * @param array|string       $ordering
     *
     * @return Token|\App\Models\BaseModel|Token|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\Relation|null|object
     */
    public function getToken($conditions = [], $withRelations = null, $limitOffset = [], $ordering = [], $orderingType = null) : ?Token
    {
        return $this
            ->getBuilder($this->prepareConditions($conditions), $withRelations, $limitOffset, $ordering, $orderingType)
            ->first();
    }

    /**
     * @param int|array|\Closure $conditions
     * @param null               $withRelations
     * @param array|string       $limitOffset
     * @param array|string       $ordering
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getTokens(
        $conditions = [],
        $withRelations = null,
        $limitOffset = [],
        $ordering = [],
        $orderingType = null
    ) : \Illuminate\Database\Eloquent\Collection {
        return $this
            ->getBuilder($this->prepareConditions($conditions), $withRelations, $limitOffset, $ordering, $orderingType, Token::class)
            ->get();
    }

    /**
     * @param int|array|\Closure $conditions
     * @param null               $withRelations
     * @param array|string       $limitOffset
     * @param array|string       $ordering
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getTokensPaginate(
        $pages = 1,
        $conditions = [],
        $withRelations = null,
        $limitOffset = [],
        $ordering = [],
        $orderingType = null
    ) : \Illuminate\Contracts\Pagination\LengthAwarePaginator {
        return $this
            ->getBuilder($this->prepareConditions($conditions), $withRelations, $limitOffset, $ordering, $orderingType, Token::class)
            ->paginate($pages);
    }

    /**
     * @param array $attributes
     *
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Support\Collection|null
     */
    public function createToken(array $attributes)
    {
        return $this->createOneOrMany($attributes, Token::class);
    }

    /**
     * @param Token $token
     * @param array   $attributes
     *
     * @return int|null
     */
    public function updateToken(Token $token, array $attributes) : ?int
    {
        return $this->updateOneAuthenticable($token, $attributes);
    }

    /**
     * @param int|array|\Closure $conditions
     * @param bool               $force
     *
     * @return bool
     */
    public function deleteToken($conditions, bool $force = false) : bool
    {
        return $this->delete($this->prepareConditions($conditions), $force, Token::class);
    }
}
