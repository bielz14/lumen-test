<?php

namespace App\Services;

use App\Models\BaseModel;
use Illuminate\Console\Command;
use Illuminate\Container\Container;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Arr;
use Illuminate\Contracts\Auth\Authenticatable;

abstract class BaseService
{
    protected $modelClassName;

    protected $modelInstance;

    public function __construct(string $className)
    {
        $this->modelClassName = $className;
        $this->modelInstance = new $className;
    }

    /**
     * @param array|\Closure $conditions
     * @param null $withRelations
     * @param array|string $limitOffset
     * @param array|string $ordering
     * @param null|string $className
     *
     * @return \Illuminate\Database\Eloquent\Model|BaseModel|Relation
     */
    public function getBuilder($conditions = [], $withRelations = null, $limitOffset = [], $ordering = [], $orderingType = null, ?string $className = null)
    {
        $limitOffset = array_filter((array)$limitOffset);
        $ordering = array_filter((array)$ordering);

        $model = $this->getInstance($className);
        if (!empty($withRelations)) {
            /** @noinspection StaticInvocationViaThisInspection */
            $model = $model->with($withRelations);
        }

        $limit = count($limitOffset) ? (int)array_shift($limitOffset) : 0;
        $offset = count($limitOffset) ? (int)array_shift($limitOffset) : 0;

        if ($limit > 0) {
            $model->limit($limit)->offset($offset);
        }

        foreach ($ordering as $field => $direction) {
            if (is_numeric($field)) {
                $field = $direction;
                if ($orderingType) {
                    $direction = $orderingType;
                } else {
                    $direction = 'desc';
                }

            }

            $model = $model->orderBy($field, $direction);
        }

        if (empty($conditions)) {
            return $model;
        }

        foreach ($conditions as $key => $condition) {
            if (is_array($condition) && count($condition) == 3) {
                $model = $model->where($condition[0], $condition[1], $condition[2]);
            } else if (is_array($condition) && isset($condition[0]) && !is_array($condition[0]) && isset($condition[1]) && is_array($condition[1])) {
                $model = $model->whereIn($condition[0], $condition[1]);
            } else {
                $model = $model->where([
                    $key => $condition
                ]);
            }
        }

        return $model;
    }

    /**
     * @param array $attributes
     * @param null|string $className
     *
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Support\Collection|null
     */
    public function createOneOrMany(array $attributes, ?string $className = null)
    {
        $model = $this->getInstance($className);
        $result = null;

        if (is_array(Arr::first($attributes))) {
            $result = collect();
            foreach ($attributes as $attribute) {
                $result->push($model::query()->create($attribute));
            }
        } else {
            $result = $model::query()->create($attributes);
        }

        return $result;
    }

    /**
     * @param Authenticatable $model
     * @param array $attributes
     *
     * @return int|null
     */
    public function updateOneAuthenticable(Authenticatable $model, array $attributes): ?int
    {
        return $model->update($attributes);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Model|BaseModel $model
     * @param array $attributes
     *
     * @return int|null
     */
    public function updateOne(BaseModel $model, array $attributes): ?int
    {
        return $model->update($attributes);
    }

    /**
     * @param array|\Closure $conditions
     * @param array $attributes
     * @param null|string $className
     *
     * @return int
     */
    public function updateMany($conditions, array $attributes, ?string $className = null): ?int
    {
        $model = $this->getInstance($className);

        return $model::query()->where($conditions)->update($attributes);//TODO: can need remove query()
    }

    /**
     * @param array|\Closure $conditions
     * @param null|bool $force
     * @param null|string $className
     *
     * @return bool
     */
    public function delete($conditions = [], ?bool $force = false, ?string $className = null): bool
    {
        /* @var $model \Illuminate\Database\Eloquent\Model */
        $model = $this->getInstance($className);
        $query = $model::query();
        if (!empty($conditions)) {
            $query = $query->where($conditions);
        }

        if ($force) {
            return (bool)$query->forceDelete();
        }

        return (bool)$query->delete();
    }

    /**
     * @param      $conditions
     * @param null $className
     *
     * @return array
     */
    protected function prepareConditions($conditions, $className = null): array
    {
        if (empty($conditions)) {
            return [];
        }

        if (!is_array($conditions) && (int)$conditions) {
            $conditions = [
                $this->getInstance($className)->getKeyName() => $conditions
            ];
        }

        return $conditions;
    }

    /**
     * This function is for realising foreign-keys dependencies in myisam-db
     *
     * @param null|string $className
     *
     * @return array
     * @throws \ReflectionException
     */
    protected function getRelated(?string $className = null): array
    {
        $relations = [];
        $reflextionClass = new \ReflectionClass($this->getClass($className));

        foreach ($reflextionClass->getMethods() as $method) {
            if (
                null === $method->getReturnType()
                || !in_array($method->getReturnType(), [
                    \Illuminate\Database\Eloquent\Relations\HasMany::class,
                    \Illuminate\Database\Eloquent\Relations\HasOne::class,
                    \Illuminate\Database\Eloquent\Relations\BelongsToMany::class,
                ])
            ) {
                continue;
            }
            $relations[$method->getName()] = $method->getReturnType()->getName();
        }

        return $relations;
    }

    /**
     * @param null|string $className
     *
     * @return null|string
     */
    private function getClass(?string $className = null): ?string
    {
        return $className ?? $this->modelClassName;
    }

    /**
     * @param null|string $className
     *
     * @return \Illuminate\Database\Eloquent\Model|BaseModel
     */
    private function getInstance(?string $className = null)
    {
        if (null !== $className && $this->modelClassName !== $className) {
            return new $className;
        }

        return $this->modelInstance;
    }

    /**
     * Set the IoC container instance.
     *
     * @param  \Illuminate\Container\Container  $container
     * @return $this
     */
    public function setContainer(Container $container)
    {
        $this->container = $container;

        return $this;
    }

    /**
     * Set the console command instance.
     *
     * @param  \Illuminate\Console\Command  $command
     * @return $this
     */
    public function setCommand(Command $command)
    {
        $this->command = $command;

        return $this;
    }
}
