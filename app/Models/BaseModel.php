<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

abstract class BaseModel extends Model
{
    public static function tableName() : string
    {
        return (new static)->getTable();
    }

    public static function className() : string
    {
        return static::class;
    }
}
