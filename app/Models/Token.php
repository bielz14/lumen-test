<?php

namespace App\Models;

/**
 * Class Token
 *
 * @property int               $id
 * @property string            $title
 * @property string            $phone
 * @property string            $description
 * @property int               $users_id
 * @property User              $user
 * @package App\Models
 */
class Token extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'access_token',
        'device_token',
        'email',
        'password',
        'user_id'
    ];

    protected $casts = [
        'id' => 'integer',
        'access_token' => 'string',
        'device_token' => 'string',
        'user_id' => 'int',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function company(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(User::class);
    }
}
