<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;

/**
 * Class Company
 *
 * @property int               $id
 * @property string            $title
 * @property string            $phone
 * @property string            $description
 * @property Collection|User   $users
 * @package App\Models
 */
class Company extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'phone',
        'description',
    ];

    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'phone' => 'int',
        'description' => 'string',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function users(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(User::class);
    }
}
