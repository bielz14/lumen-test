<?php

namespace App\Http\Middleware;

use App\Services\UserService;
use Closure;
use Exception;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;

class JwtMiddleware
{
    public $service;

    /**
     * @param UserService $service
     */
    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    public function handle($request, Closure $next, $guard = null)
    {
        $token = $request->get('token');

        if(!$token) {
            // Unauthorized response if token not there
            return response()->json([
                'status' => 'error',
                'message' => 'Token not provided.'
            ], 401);
        }

        try {
            $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
        } catch(ExpiredException $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Provided token is expired.'
            ], 401);
        } catch(Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'An error while decoding token.'
            ], 401);
        }

        $tokens = $this->service->getTokens()
            ->pluck('access_token', 'user_id')
            ->toArray();

        if ($userId = array_search($token, $tokens)) {
            $user = $this->service->getUser($userId);
            $request->auth = $user;
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Token not authorized.'
            ], 422);
        }

        return $next($request);
    }
}
