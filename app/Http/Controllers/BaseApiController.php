<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class BaseApiController extends Controller
{
    /**
     * @param array       $data
     * @param null|string $message
     * @param bool|null   $isSuccess
     *
     * @return JsonResponse
     */
    public function response(?array $data = null, ?string $message = null, ?bool $isSuccess = null, $statusCode) : JsonResponse
    {
        if ($isSuccess) {
            $status = 'success';
        } else {
            $status = 'error';
        }
        $dataToReturn = [
            'status' => $status,
            'message' => $message,
            'data' => $data
        ];

        return new JsonResponse($dataToReturn, $statusCode);
    }
}
