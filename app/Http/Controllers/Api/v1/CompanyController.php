<?php

namespace App\Http\Controllers\Api\v1;

use App\Services\CompanyService;
use App\Http\Controllers\BaseApiController;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CompanyController extends BaseApiController
{
    public $service;

    /**
     * @param CompanyService $service
     */
    public function __construct(CompanyService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $list = $this->service->getCompanies();
        return $this->response(['companies' => $list], 'Companies list.', true, 200);
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string|unique:companies',
            'phone' => 'required|string|max:255|unique:users|unique:companies',
            'description' => 'required|string',
        ]);

        /* @var $service CompanyService */
        try {
            DB::beginTransaction();

            $attributes = $request->all();

            $company = $this->service->createCompany($attributes);
            if ($company) {
                $company = $company->toArray();
            }

            DB::commit();

            return $this->response(['company' => $company], 'Company created.', true, 200);
        } catch (\Exception $e) {

            DB::rollBack();

            return $this->response(null, $e->getMessage(), false, 400);
        }
    }
}
