<?php

namespace App\Http\Controllers\Api\v1;

use App\Services\UserService;
use App\Http\Controllers\BaseApiController;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Password;
use Illuminate\Auth\Passwords\PasswordBrokerManager;
use Laravel\Lumen\Routing\Controller;

class AuthController extends BaseApiController
{
    const DEFAULT_COMPANY_ID = 1;

    public $service;

    /**
     * @param UserService $service
     */
    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'phone' => 'required|string|max:255|unique:users|unique:companies',
            'password' => 'string|confirmed|required|same:password_confirmation',
            'password_confirmation' => 'min:6|required_with:password',
            'company_id' => 'integer',
            $this->username() => 'required|email|unique:users'
        ]);

        /* @var $service UserService */
        try {
            DB::beginTransaction();

            $attributes = $request->all();
            $attributes['password'] = Hash::make($attributes['password']);
            if (!isset($attributes['company_id'])) {
                $attributes['company_id'] = self::DEFAULT_COMPANY_ID;
            }

            $user = $this->service->createUser($attributes);
            if ($user) {
                $user = $user->toArray();
            }

            DB::commit();

            return $this->response(['user' => $user], 'User registration.', true, 200);
        } catch (\Exception $e) {

            DB::rollBack();

            return $this->response(null, $e->getMessage(), false, 400);
        }
    }

    public function username()
    {
        return 'email';
    }

    /**
     * Create a new token.
     *
     * @param  \App\Models\User   $user
     * @return string
     */
    protected function jwt(Authenticatable $user) {
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued.
            'exp' => time() + 60*60 // Expiration time
        ];

        // As you can see we are passing `JWT_SECRET` as the second parameter that will
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required|email',
            'password' => 'required|string',
            'device_token' => 'required|string'
        ]);

        $email = $request->input('email');
        $password = $request->input('password');

        $user = $this->service->getUser(['email' => $email]);
        if(!$user) {
            return $this->response(null, 'User not found.', false,422);
        }

        if (Hash::check($password, $user->password)) {
            $accessToken = $this->jwt($user);
            $deviceToken = $request->input('device_token');
            $this->service->createToken([
                'user_id' => $user->id,
                'access_token' => $accessToken,
                'device_token' => $deviceToken
            ]);

            return $this->response(['token' => $accessToken], 'Success auth.', true,200);
        }

        return $this->response(null, 'Invalid Credential.', false,401);
    }

    public function logout(Request $request)
    {
        $token = $request->get('token');
        $status = $this->service->deleteToken(['access_token' => $token]);

        if ($status) {
            return $this->response(null, 'Successful logout.', $status,200);
        } else {
            return $this->response(null, 'Technical problems with logout.', $status,400);
        }
    }

    // Send reset password email
    public function generateResetToken(Request $request)
    {
        $this->validate($request, [$this->username() => 'required|email']);

        $response = $this->broker()->sendResetLink(
            $request->only($this->username())
        );

        return $response == Password::RESET_LINK_SENT
            ? $this->response(null, 'Successful send mail.', true,200)
            : $this->response(null, 'Technical problems with send mail.', false,400);
    }

    public function resetPassword(Request $request)
    {
        $rules = [
            'token'    => 'required',
            $this->username() => 'required|email',
            'password' => 'required|min:6',
        ];

        $this->validate($request, $rules);

        // Reset the password
        $response = $this->broker()->reset(
        $this->credentials($request),
            function ($user, $password) {
                $user->password = Hash::make($password);
                $user->save();//TODO: change way on way of service
            }
        );

        return $response == Password::PASSWORD_RESET
            ? $this->response(null, 'Successful reset password.', true,200)
            : $this->response(null, 'Technical problems with reset password.', false,400);
    }

    /**
     * Get the password reset credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password', 'password_confirmation', 'token');
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        $passwordBrokerManager = new PasswordBrokerManager(app());
        return $passwordBrokerManager->broker();
    }
}
