<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Services\CompanyService;

class DefaultDataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        $this
            ->createCompanies()
        ;
        DB::commit();
    }

    public function createCompanies()
    {
        /* @var $service CompanyService */
        $service = $this->resolve(CompanyService::class);
        $dataToCreate = [
            [
                'title' => 'First test company',
                'phone' => '9379992',
                'description' => 'text fish to description',
            ],
        ];
        foreach ($dataToCreate as $data) {
            $service->createCompany($data);
        }

        return $this;
    }
}
